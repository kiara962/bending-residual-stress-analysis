
#ifndef DATA_H_
#define DATA_H_

#include <string>
#include <vector>

struct AnalysisData {
  double thickness_;     //板厚
  double r0_;            //負荷時の曲率半径
  double r1_;            //除荷時の曲率半径
  int n_div_;            //板厚方向離散化の分割数
  int output_interval_;  //出力の間隔
  std::vector<std::vector<double>> ss_table = {};  //応力ひずみ関係
  std::vector<double> stress_distribution = {};    //応力分布
};

#endif /* DATA_H_ */
