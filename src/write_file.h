
#ifndef WRITEFILE_H_
#define WRITEFILE_H_

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "data.h"

void writeFile(std::string output_filename, AnalysisData& data) {
  int i = 0;
  double y;
  std::cout << "Writing " << output_filename << std::endl;

  std::ofstream ofs;
  ofs.open(output_filename, std::ios::out);

  ofs << 0 << "," << 0 << std::endl;
  for (i = 1; i <= data.n_div_ / data.output_interval_; i++) {
    y = (double)i * data.output_interval_ / (double)data.n_div_ *
        (data.thickness_ / 2);
    ofs << y << "," << data.stress_distribution[i * data.output_interval_ - 1]
        << std::endl;
  }
  for (i = 1; i <= data.n_div_ / data.output_interval_; i++) {
    y = -(double)i * data.output_interval_ / (double)data.n_div_ *
        (data.thickness_ / 2);
    ofs << y << "," << -data.stress_distribution[i * data.output_interval_ - 1]
        << std::endl;
  }
  ofs.close();
  std::cout << "finished" << std::endl;
}

#endif /* WRITEFILE_H_ */
