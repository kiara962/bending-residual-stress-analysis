
#include <cmath>
#include <iostream>
#include <string>
#include <vector>

#include "data.h"
#include "read_file.h"
#include "write_file.h"

using namespace std;

//ひずみに対して応力を返す関数
double stressStrain(double strain, AnalysisData &data) {
  int i;
  if (strain >= 0) {  //ひずみが正の場合
    for (i = data.ss_table.size() - 1; i >= 0; i--) {
      if (strain > data.ss_table[i][2]) {
        return (data.ss_table[i][0] +
                data.ss_table[i][1] * 1000 * (strain - data.ss_table[i][2]));
      }
    }
  } else {  //ひずみが負の場合
    return data.ss_table[0][1] * 1000 * strain;
  }
}

//残留曲げモーメントを求める関数
double resMoment(double r0, AnalysisData &data) {
  int i;
  double M = 0;
  double strain;
  double y;
  for (i = 1; i <= data.n_div_; i++) {
    y = (double)i / (double)data.n_div_ * (data.thickness_ / 2);
    strain = y / r0;  //負荷時
    double sa = stressStrain(strain, data);
    double b = sa / data.ss_table[0][1] / 1000;
    strain = (y / r0) - (y / data.r1_) - b;  //除荷後
    data.stress_distribution[i - 1] = stressStrain(strain, data);
    M += data.stress_distribution[i - 1] * y *
         (data.thickness_ / 2 / data.n_div_);
  }
  M *= 2;
  return M;
}

//曲げ後の残留モーメントが0となるようなr0を反復計算で求める関数
//計算手法ははさみうち法
//初期値a,bの間で解を探索する
void solve(AnalysisData &data) {
  double a = 0.001;     //初期値a
  double b = data.r1_;  //初期値b
  int counter = 0;
  int max_itr = 1000;  //反復回数上限

  std::cout << "Solveing" << endl;
  do {
    if (counter >= max_itr) {
      cout << "The maximum number of iterations has been reached." << endl;
      exit(0);
    }
    data.r0_ = (a * resMoment(b, data) - b * resMoment(a, data)) /
               (resMoment(b, data) - resMoment(a, data));
    if (resMoment(data.r0_, data) == 0) {
      break;
    } else if (resMoment(a, data) * resMoment(data.r0_, data) < 0) {
      b = data.r0_;
    } else if (resMoment(a, data) * resMoment(data.r0_, data) > 0) {
      a = data.r0_;
    }
    cout << "r0= " << data.r0_ << ", Rm=" << resMoment(data.r0_, data)
         << std::endl;
    counter++;
  } while (fabs(resMoment(data.r0_, data)) > 0.001);
  cout << "r0= " << data.r0_ << ", Rm=" << resMoment(data.r0_, data)
       << std::endl;
}

int main(int argc, char *argv[]) {
  if (argc != 3) {
    cout << "Please input [input filename] [output filename]" << endl;
    exit(0);
  }
  std::string input_filename = argv[1];
  std::string output_filename = argv[2];
  AnalysisData data;

  readFile(input_filename, data);
  solve(data);
  writeFile(output_filename, data);

  return 0;
}
