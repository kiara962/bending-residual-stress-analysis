
#ifndef READFILE_H_
#define READFILE_H_

#include <fstream>
#include <iostream>
#include <string>
#include <vector>

#include "data.h"

void readFile(std::string input_filename, AnalysisData& data) {
  int i = 0, j = 0, k;
  int counter = 0;
  std::vector<double> temp = {0, 0, 0};

  std::ifstream ifs;
  std::string reading_line_buffer;
  ifs.open(input_filename, std::ios::in);
  if (ifs.fail()) {
    std::cerr << "File do not exist.\n";
    exit(0);
  }
  std::cout << "Reading input file" << std::endl;

  getline(ifs, reading_line_buffer);
  while (!ifs.eof()) {
    if (reading_line_buffer.find("!MATERIAL") != std::string::npos) {
      for (;;) {
        try {
          getline(ifs, reading_line_buffer);
          if (reading_line_buffer.find("!") != std::string::npos ||
              ifs.eof() == true) {
            i = 0;
            j = 0;
            break;
          }
          j = reading_line_buffer.find(",", 0);
          temp[0] = stod(reading_line_buffer.substr(0, j));
          i = j;
          j = reading_line_buffer.find(", ", i);
          temp[1] = stod(reading_line_buffer.substr(i + 1, j - i - 1));
          data.ss_table.push_back(temp);
          i = 0;
          j = 0;
        } catch (const std::invalid_argument& e) {
          std::cout << "invalid argument in !MATERIAL" << std::endl;
          exit(0);
        }
      }
      for (k = 0; k < data.ss_table.size(); k++) {
        std::cout << data.ss_table[k][0] << "," << data.ss_table[k][1] << ","
                  << data.ss_table[k][2] << std::endl;
      }
      counter++;

    } else if (reading_line_buffer.find("!RADIUS") != std::string::npos) {
      getline(ifs, reading_line_buffer);
      try {
        data.r1_ = stod(reading_line_buffer);
      } catch (const std::invalid_argument& e) {
        std::cout << "invalid argument in !RADIUS" << std::endl;
        exit(0);
      }
      std::cout << "r1= " << data.r1_ << std::endl;
      getline(ifs, reading_line_buffer);
      counter++;

    } else if (reading_line_buffer.find("!THICKNESS") != std::string::npos) {
      getline(ifs, reading_line_buffer);
      try {
        data.thickness_ = stod(reading_line_buffer);
      } catch (const std::invalid_argument& e) {
        std::cout << "invalid argument in !THICKNESS" << std::endl;
        exit(0);
      }
      std::cout << "thickness= " << data.thickness_ << std::endl;
      getline(ifs, reading_line_buffer);
      counter++;

    } else if (reading_line_buffer.find("!NUM_DIV") != std::string::npos) {
      getline(ifs, reading_line_buffer);
      try {
        data.n_div_ = stod(reading_line_buffer);
      } catch (const std::invalid_argument& e) {
        std::cout << "invalid argument in !NUM_DIV" << std::endl;
        exit(0);
      }
      std::cout << "n_div= " << data.n_div_ << std::endl;
      getline(ifs, reading_line_buffer);
      counter++;

    } else if (reading_line_buffer.find("!OUTPUT_INTERVAL") !=
               std::string::npos) {
      getline(ifs, reading_line_buffer);
      try {
        data.output_interval_ = stod(reading_line_buffer);
      } catch (const std::invalid_argument& e) {
        std::cout << "invalid argument in !OUTPUT_INTERVAL" << std::endl;
        exit(0);
      }
      std::cout << "output_interval= " << data.output_interval_ << std::endl;
      getline(ifs, reading_line_buffer);
      counter++;

    } else {
      getline(ifs, reading_line_buffer);
    }
  }


  if (counter != 5) {
    std::cerr << "Faild reading input file" << std::endl;
    exit(0);
  }
  ifs.close();
  std::cout << "Reading file finished" << std::endl;

  // set yeild strain
  for (i = 1; i < data.ss_table.size(); i++) {
    data.ss_table[i][2] = ((data.ss_table[i][0] - data.ss_table[i - 1][0]) /
                           data.ss_table[i - 1][1] / 1000) +
                          data.ss_table[i - 1][2];
  }

  for (i = 0; i < data.n_div_; i++) {
    data.stress_distribution.push_back(0);
  }
}
#endif /* READFILE_H_ */
