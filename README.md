#  bending-residual-stress-analysis
板材の冷間曲げ加工後の残留応力分布を計算する  

### ・コンパイル
g++ src/main.cpp -o BendResStress 

### ・実行
./BendResStress sample.dat out.csv  
入力ファイル名と出力ファイル名を指定 


### ・入力ファイル
sammple.datを参照  
!MATERIAL ：材料特性（多直線近似塑性）降伏点と接線剛性係数を入力  
!RADIUS  　：曲率半径  
!THICKNESS：板厚  
!NUM_DIV   ：板厚方向離散化の分割数  
!OUTPUT_INTERVAL：出力の間隔  

### ・出力ファイル
板厚方向の座標，残留応力の値  
